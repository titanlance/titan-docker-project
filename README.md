# Titan Docker Project

All docker related files for Titan/Lumerin.

## Getting started

Find the folder with the docker project you need.

## Authors
If you contribute, make sure you make it known and even add yourself to the Authors here!

Lance Seidman - Smart Contract Developer

## License
MIT only projects here!

