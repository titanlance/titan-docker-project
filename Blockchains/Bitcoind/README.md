# Bitcoin Daemon

# Run Bitcoind
- Create Bitcoin Node Volume

docker volume create --name=bitcoind-data

- Get the latest Bitcoin image from kylemanna & run container

docker run -v bitcoind-data:/bitcoin/.bitcoin --name=bitcoind-node -d -p 8333:8333 -p <local-IP>:8332:8332 kylemanna/bitcoind

# Find Container ID/Name
docker ps

# Config (Also get Bitcoind RPC User/Pass)
docker exec CONTAINER-ID cat .bitcoin/bitcoin.conf

# Testing
- Is Port Public?

docker port bitcoind-node 8332

- Docker Test

docker exec CONTAINER-ID bitcoin-cli getmininginfo

- RPC Test via cURL

Get the Blockchain Info

curl --data-binary '{"jsonrpc":"1.0","id":"curltest","method":"getblockchaininfo","params":[]}' -H 'content-type:text/plain;' http://bitcoinrpc:RPC-PASSWORD@IP-ADDRESS:8332/

# Troubleshooting
Follow/monitor Container logs in Real Time...

docker logs -f bitcoind-node
